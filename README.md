# Projeto Tech Day do Bootcamp "Cloud DevOps Experience - Banco Carrefour"

## Características da aplicação:

Projeto Base: [Repositório](https://github.com/digitalinnovationone/banco-carrefour-techday-cloud-devops)

-Frontend (HTML/CSS/JS)
-Backend (PHP e MySQL)

## Pré-requisitos para rodar a aplicação:

1. Cluster Kubernetes.
2. VM (Bastion Host).
3. Ter uma instância NFS (Ex.: Google File Store).

## Instruções para uso:

1. Configurar o bastion host da seguinte maneira:

   ```bash
    #Configurar conta
    gcloud config set account <email@email.com>
    gcloud auth login
    #Acessar link fornecido, copiar código e colar no espaço solicitado
    #Instalar kubectl
    sudo apt-get install kubectl -y
    #Instalar git
    sudo apt-get install git -y
    #Instalar plugin de autenticação
    sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
    #Configurar acesso ao cluster
    <Colar linha de comando fornecida para realizar a conexão com o cluster>
   ```

2. Configurar variáveis no gitlab. As seguintes variáveis são utilizadas:
   1. MYSQL_DATABASE=meubanco
   2. MYSQL_ROOT_PASSWORD=(Senha do BD)
   3. REGISTRY_PASS=(Nome de usuário Dockerhub)
   4. REGISTRY_USER=(Senha de usuário Dockerhub)
   5. SSH_SERVER=(ip bastion host)
   6. SSH_KEY=(chave privada de acesso ao bastion host)

3. Aplicar as configurações de service e secrets no cluster previamente à inicialização do CI/CD.
4. Após realizar o passo anterior, busque o IP público obtido pelo loadBalancer, executando o comando ```kubectl get services``` no bastion host, e o insira no arquivo app/js.js. Conforme as imagens abaixo:
   ![Imagem1](assets/loadBalancerIP.png)
   ![Imagem2](assets/arquivoJS.JS.png)
5. A configuração está pronta, basta realizar um push para o repositório e acompanhar as pipelines.
6. A página web do projeto estará disponível no mesmo ip copiado no passo 4.

---

<h1>Repositório dos arquivos do projeto base para o projeto Tech Day do Bootcamp "Cloud DevOps Experience - Banco Carrefour"</h1>

<h2>Características da aplicação:</h2> 

•	Frontend (HTML/CSS/JS)<br>
•	Backend (PHP e MySQL)

<h3>O participante é livre para alterar as linguagens de programação e/ou realizar melhorias no projeto, desde que atenda às exigências de entrega.</h3>

O participante deverá criar um pipeline de deploy desta aplicação em forma de Containers em um cluster Kubernetes. O participante poderá utilizar um cluster
kubernetes em nuvem (Preferencialmente utilizando GCP).

<h2>Entrega do projeto</h2>

Com o problema apresentado, o participante poderá entrar o seu pipeline de CI/CD utilizando o Gitlab, Terraform ou outra solução desejada, como o exemplo abaixo:

https://gitlab.com/denilsonbonatti/app-cicd-dio/-/blob/main/.gitlab-ci.yml

O participante deverá apresentar um vídeo com o pit da sua solução com no máximo 5 minutos.

Link do formulário de entrega:

https://forms.office.com/r/S8MHD0HrUb
